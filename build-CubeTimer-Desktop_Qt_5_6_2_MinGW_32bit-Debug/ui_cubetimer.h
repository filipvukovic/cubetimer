/********************************************************************************
** Form generated from reading UI file 'cubetimer.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUBETIMER_H
#define UI_CUBETIMER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CubeTimer
{
public:
    QWidget *centralWidget;
    QSplitter *splitter;
    QLCDNumber *timeLcd_mm;
    QLabel *label;
    QLCDNumber *timeLcd_ss;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *startBtn;
    QHBoxLayout *horizontalLayout;
    QPushButton *stopBtn;
    QWidget *widget;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *timesLabel;
    QPushButton *clearBtn;
    QListWidget *timesList;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *avgLabel;
    QLabel *avg5Label;
    QHBoxLayout *horizontalLayout_4;
    QLabel *avgLabel2;
    QLabel *avg12Label;
    QWidget *widget1;
    QVBoxLayout *verticalLayout_4;
    QLabel *scrambleLbl;
    QLabel *scrambleLabel;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *CubeTimer)
    {
        if (CubeTimer->objectName().isEmpty())
            CubeTimer->setObjectName(QStringLiteral("CubeTimer"));
        CubeTimer->resize(592, 460);
        centralWidget = new QWidget(CubeTimer);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setGeometry(QRect(250, 150, 271, 141));
        splitter->setOrientation(Qt::Horizontal);
        timeLcd_mm = new QLCDNumber(splitter);
        timeLcd_mm->setObjectName(QStringLiteral("timeLcd_mm"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(timeLcd_mm->sizePolicy().hasHeightForWidth());
        timeLcd_mm->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(8);
        font.setKerning(true);
        timeLcd_mm->setFont(font);
        timeLcd_mm->setFrameShape(QFrame::NoFrame);
        timeLcd_mm->setDigitCount(2);
        splitter->addWidget(timeLcd_mm);
        label = new QLabel(splitter);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setPointSize(25);
        label->setFont(font1);
        splitter->addWidget(label);
        timeLcd_ss = new QLCDNumber(splitter);
        timeLcd_ss->setObjectName(QStringLiteral("timeLcd_ss"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(timeLcd_ss->sizePolicy().hasHeightForWidth());
        timeLcd_ss->setSizePolicy(sizePolicy1);
        timeLcd_ss->setFrameShape(QFrame::NoFrame);
        timeLcd_ss->setFrameShadow(QFrame::Raised);
        timeLcd_ss->setDigitCount(5);
        timeLcd_ss->setMode(QLCDNumber::Dec);
        timeLcd_ss->setSegmentStyle(QLCDNumber::Filled);
        splitter->addWidget(timeLcd_ss);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(310, 310, 160, 27));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        startBtn = new QPushButton(layoutWidget);
        startBtn->setObjectName(QStringLiteral("startBtn"));

        horizontalLayout_2->addWidget(startBtn);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        stopBtn = new QPushButton(layoutWidget);
        stopBtn->setObjectName(QStringLiteral("stopBtn"));

        horizontalLayout->addWidget(stopBtn);


        horizontalLayout_2->addLayout(horizontalLayout);

        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 120, 201, 281));
        verticalLayout_3 = new QVBoxLayout(widget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        timesLabel = new QLabel(widget);
        timesLabel->setObjectName(QStringLiteral("timesLabel"));
        QFont font2;
        font2.setPointSize(16);
        timesLabel->setFont(font2);

        verticalLayout->addWidget(timesLabel);

        clearBtn = new QPushButton(widget);
        clearBtn->setObjectName(QStringLiteral("clearBtn"));

        verticalLayout->addWidget(clearBtn);

        timesList = new QListWidget(widget);
        timesList->setObjectName(QStringLiteral("timesList"));
        QFont font3;
        font3.setPointSize(15);
        timesList->setFont(font3);

        verticalLayout->addWidget(timesList);


        verticalLayout_3->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        avgLabel = new QLabel(widget);
        avgLabel->setObjectName(QStringLiteral("avgLabel"));
        QFont font4;
        font4.setPointSize(13);
        avgLabel->setFont(font4);
        avgLabel->setFrameShape(QFrame::NoFrame);
        avgLabel->setFrameShadow(QFrame::Plain);

        horizontalLayout_3->addWidget(avgLabel);

        avg5Label = new QLabel(widget);
        avg5Label->setObjectName(QStringLiteral("avg5Label"));
        avg5Label->setFont(font4);

        horizontalLayout_3->addWidget(avg5Label);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        avgLabel2 = new QLabel(widget);
        avgLabel2->setObjectName(QStringLiteral("avgLabel2"));
        avgLabel2->setFont(font4);

        horizontalLayout_4->addWidget(avgLabel2);

        avg12Label = new QLabel(widget);
        avg12Label->setObjectName(QStringLiteral("avg12Label"));
        avg12Label->setFont(font4);

        horizontalLayout_4->addWidget(avg12Label);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout_3->addLayout(verticalLayout_2);

        widget1 = new QWidget(centralWidget);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setGeometry(QRect(20, 11, 561, 91));
        verticalLayout_4 = new QVBoxLayout(widget1);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        scrambleLbl = new QLabel(widget1);
        scrambleLbl->setObjectName(QStringLiteral("scrambleLbl"));
        scrambleLbl->setFont(font2);

        verticalLayout_4->addWidget(scrambleLbl);

        scrambleLabel = new QLabel(widget1);
        scrambleLabel->setObjectName(QStringLiteral("scrambleLabel"));
        QFont font5;
        font5.setPointSize(12);
        scrambleLabel->setFont(font5);

        verticalLayout_4->addWidget(scrambleLabel);

        CubeTimer->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CubeTimer);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 592, 21));
        CubeTimer->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CubeTimer);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CubeTimer->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CubeTimer);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CubeTimer->setStatusBar(statusBar);

        retranslateUi(CubeTimer);

        QMetaObject::connectSlotsByName(CubeTimer);
    } // setupUi

    void retranslateUi(QMainWindow *CubeTimer)
    {
        CubeTimer->setWindowTitle(QApplication::translate("CubeTimer", "CubeTimer", 0));
        label->setText(QApplication::translate("CubeTimer", ":", 0));
        startBtn->setText(QApplication::translate("CubeTimer", "Start ", 0));
        stopBtn->setText(QApplication::translate("CubeTimer", "Stop", 0));
        timesLabel->setText(QApplication::translate("CubeTimer", "Times", 0));
        clearBtn->setText(QApplication::translate("CubeTimer", "Clear", 0));
        avgLabel->setText(QApplication::translate("CubeTimer", "Average of 5 :", 0));
        avg5Label->setText(QString());
        avgLabel2->setText(QApplication::translate("CubeTimer", "Average of 12:", 0));
        avg12Label->setText(QString());
        scrambleLbl->setText(QApplication::translate("CubeTimer", "3x3 Scramble : ", 0));
        scrambleLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CubeTimer: public Ui_CubeTimer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUBETIMER_H
