#ifndef CUBETIMER_H
#define CUBETIMER_H

#include <QMainWindow>
#include <QtCore>
#include <QLCDNumber>
#include <QTimer>
#include <QDateTime>
#include <QKeyEvent>
#include <QMessageBox>
#include <QVector>


namespace Ui {
class CubeTimer;
}

class CubeTimer : public QMainWindow
{
    Q_OBJECT

public:
    explicit CubeTimer(QWidget *parent = 0);
    ~CubeTimer();
public slots:
    void startTimer();
private slots:
    void on_startBtn_clicked();
    void update_time();
    void on_stopBtn_clicked();

    void on_timesList_clicked(const QModelIndex &index);

    void on_clearBtn_clicked();

private:
    Ui::CubeTimer *ui;
    QTimer *cube_timer;
    QTime myTimer;
    int list_count;
    int ao5;
    int ao12;
    void update_average();
    void scramble();
    QVector<int> time_vector;
    QString SCR[19];
    void scramble_moves();
};

#endif // CUBETIMER_H
