#-------------------------------------------------
#
# Project created by QtCreator 2017-09-20T16:26:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CubeTimer
TEMPLATE = app


SOURCES += main.cpp\
        cubetimer.cpp

HEADERS  += cubetimer.h

FORMS    += cubetimer.ui
