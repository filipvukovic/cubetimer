#include "cubetimer.h"
#include "ui_cubetimer.h"

CubeTimer::CubeTimer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CubeTimer)
{
    ui->setupUi(this);
    list_count = 0;
    ao5 = 0;
    ao12 = 0;
    ui->stopBtn->setEnabled(false);
    cube_timer = new QTimer(this);
    ui->avg5Label->setText("-.-");
    ui->avg12Label->setText("-.-");
    scramble_moves();
    scramble();

}



CubeTimer::~CubeTimer()
{
    delete ui;
    delete cube_timer;

}

void CubeTimer::startTimer()
{
    myTimer.start();
}


void CubeTimer::on_startBtn_clicked()
{
    startTimer();
    ui->stopBtn->setEnabled(true);
    ui->startBtn->setEnabled(false);
    connect(cube_timer, SIGNAL(timeout()), this, SLOT(update_time()));
    cube_timer->start(100);
}

void CubeTimer::update_time()
{
    int time = myTimer.elapsed();
    int mm = time/ 60000;
    int temp_ss = (time%60000);
    double ss = (float)temp_ss / 1000;
    QString seconds = QString::number(ss,'f', 2);
    QString minutes = QString::number(mm);
    ui->timeLcd_ss->display(seconds);
    ui->timeLcd_mm->display(minutes);
}

void CubeTimer::on_stopBtn_clicked()
{
    int time = myTimer.elapsed();
    time_vector.push_back(time);
    int mm = time/ 60000;
    int temp_ss = (time%60000);
    double ss = (float)temp_ss / 1000;
    QString seconds = QString::number(ss,'f', 2);
    QString minutes = QString::number(mm);
    ui->timeLcd_ss->display(seconds);
    ui->timeLcd_mm->display(minutes);
    ui->stopBtn->setEnabled(false);
    ui->startBtn->setEnabled(true);
    cube_timer->stop();
    if(mm==0)
        ui->timesList->addItem(seconds);
    else
        ui->timesList->addItem(minutes + ":" + seconds);
    list_count ++;
    if(list_count <= 5)
        ao5 += time;
    if(list_count <= 12)
        ao12 += time;
    update_average();
    scramble();
}

void CubeTimer::update_average()
{
    if(list_count < 5){
        ui->avg5Label->setText("-.-");
        return;
    }
    if(list_count < 12){
        ui->avg12Label->setText("-.-");
    }
    if(list_count ==5){
        ao5 /= 5;
        int mm = ao5/ 60000;
        int temp_ss = (ao5%60000);
        double ss = (float)temp_ss / 1000;
        QString seconds = QString::number(ss,'f', 3);
        QString minutes = QString::number(mm);
        if (mm==0)
            ui->avg5Label->setText(seconds);
        else
            ui->avg5Label->setText(minutes + ":" + seconds);
    }
    if(list_count == 12){
        ao12 /= 12;
        int mm = ao12/ 60000;
        int temp_ss = (ao12%60000);
        double ss = (float)temp_ss / 1000;
        QString seconds = QString::number(ss,'f', 3);
        QString minutes = QString::number(mm);
        if (mm==0)
            ui->avg12Label->setText(seconds);
        else
            ui->avg12Label->setText(minutes + ":" + seconds);
    }

}

void CubeTimer::scramble()
{
      QString scramble;
      for(int i = 0; i<25; i++){
          scramble += SCR[rand() %19];
          scramble += " ";
      }
      ui->scrambleLabel->setText(scramble);
}

void CubeTimer::scramble_moves()
{
    SCR[0] = "U";
    SCR[1] = "U´";
    SCR[2] = "D";
    SCR[3] = "D´";
    SCR[4] = "F";
    SCR[5] = "F´";
    SCR[6] = "B";
    SCR[7] = "B´";
    SCR[8] = "R";
    SCR[9] = "R´";
    SCR[10] = "L";
    SCR[12] = "L´";
    SCR[13] = "U2";
    SCR[14] = "D2";
    SCR[15] = "F2";
    SCR[16] = "B2";
    SCR[17] = "R2";
    SCR[18] = "L2";
}

void CubeTimer::on_timesList_clicked(const QModelIndex &index)
{
    int index_on_list = index.row();
    QMessageBox msgBox;
    msgBox.setWindowTitle("Options");
    QAbstractButton* pButtonRemove = msgBox.addButton(tr("Remove"), QMessageBox::YesRole);
    QAbstractButton* pButtonClose = msgBox.addButton(tr("Close"), QMessageBox::YesRole);
    msgBox.exec();


    if(msgBox.clickedButton()==pButtonRemove){
        ui->timesList->takeItem(index_on_list);
        list_count --;
        if(list_count <=5)
            ao5 -= time_vector[index_on_list];
        if(list_count <=12)
            ao12 -= time_vector[index_on_list];
        time_vector.remove(index_on_list);
        update_average();
    }
    if(msgBox.clickedButton()==pButtonClose){
        msgBox.close();
    }

}

void CubeTimer::on_clearBtn_clicked()
{
    ui->timesList->clear();
    ao5 = 0;
    ao12 = 0;
    time_vector.clear();
    list_count = 0;
    ui->avg5Label->setText("-.-");
    ui->avg12Label->setText("-.-");
    ui->timeLcd_ss->display("0");
    ui->timeLcd_mm->display("0");
}
